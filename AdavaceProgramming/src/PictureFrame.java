import java.awt.*;

import javax.swing.*;

public class PictureFrame {
  public int[] reroll = null;
  Aardvark master = null;
  public static final int ColourValue = 20;
  public static final int DrawValue = 30;
  class DominoPanel extends JPanel {
    private static final long serialVersionUID = 4190229282411119364L;

    public void drawGrid(Graphics g) {
      for (int are = 0; are < 7; are++) {
        for (int see = 0; see < 8; see++) {
          drawDigitGivenCentre(new DrawDigitGivenCentreParameter(g, DrawValue + see * ColourValue, DrawValue+ are * ColourValue, ColourValue, master.data.grid[are][see]));
        }
      }
    }

    public void drawGridLines(Graphics g) {
      g.setColor(Color.LIGHT_GRAY);
      for (int are = 0; are <= 7; are++) {
        g.drawLine(ColourValue, ColourValue + are * ColourValue, 180, ColourValue + are * ColourValue);
      }
      for (int see = 0; see <= 8; see++) {
        g.drawLine(ColourValue + see * ColourValue, ColourValue,ColourValue + see * ColourValue, 160);
      }
    }

    public void drawHeadings(Graphics g) {
      for (int are = 0; are < 7; are++) {
        fillDigitGivenCentre(g, 10, DrawValue + are * ColourValue, ColourValue, are+1);
      }

      for (int see = 0; see < 8; see++) {
        fillDigitGivenCentre(g, DrawValue + see * ColourValue, 10, ColourValue, see+1);
      }
    }

    public void drawDomino(Graphics g, Domino d) {
      if (d.placed) {
        int y = Math.min(d.ly, d.hy);
        int x = Math.min(d.lx, d.hx);
        int w = Math.abs(d.lx - d.hx) + 1;
        int h = Math.abs(d.ly - d.hy) + 1;
        g.setColor(Color.WHITE);
        g.fillRect(ColourValue + x * ColourValue, ColourValue + y * ColourValue, w * ColourValue, h * ColourValue);
        g.setColor(Color.RED);
        g.drawRect(ColourValue + x * ColourValue, ColourValue + y * ColourValue, w * ColourValue, h * ColourValue);
        drawDigitGivenCentre(g, DrawValue + d.hx * ColourValue, DrawValue + d.hy * ColourValue, ColourValue, d.high,
            Color.BLUE);
        drawDigitGivenCentre(g, DrawValue + d.lx * ColourValue, DrawValue + d.ly * ColourValue, ColourValue, d.low,
            Color.BLUE);
      }
    }

    void drawDigitGivenCentre(DrawDigitGivenCentreParameter parameterObject) {
      int radius = parameterObject.diameter / 2;
      parameterObject.g.setColor(Color.BLACK);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = parameterObject.g.getFontMetrics();
      String txt = Integer.toString(parameterObject.n);
      parameterObject.g.drawString(txt, parameterObject.x - fm.stringWidth(txt) / 2, parameterObject.y + fm.getMaxAscent() / 2);
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n,Color c) {
      int radius = diameter / 2;
      g.setColor(c);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }
    
    void fillDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.GREEN);
      g.fillOval(x - radius, y - radius, diameter, diameter);
      g.setColor(Color.BLACK);
      g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    protected void paintComponent(Graphics g) {
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, getWidth(), getHeight());

      // numbaz(g);
      //
      // if (master!=null && master.orig != null) {
      // drawRoll(g, master.orig);
      // }
      // if (reroll != null) {
      // drawReroll(g, reroll);
      // }
      //
      // drawGrid(g);
      if (master.data.mode == 1) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawGuesses(g);
      }
      if (master.data.mode == 0) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawDominoes(g);
      }
    }

    public Dimension getPreferredSize() {
      return new Dimension(202, 182);
    }
  }

  public DominoPanel dp;

  public void PictureFrame(Aardvark sf) {
    master = sf;
    if (dp == null) {
      JFrame f = new JFrame("Abominodo");
      dp = new DominoPanel();
      f.setContentPane(dp);
      f.pack();
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.setVisible(true);
    }
  }

  public void reset() {
    // TODO Auto-generated method stub

  }

}
