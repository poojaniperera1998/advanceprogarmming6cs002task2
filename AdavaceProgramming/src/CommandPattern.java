
public abstract class CommandPattern {
	public Aardvark editor;
    public String xbackup;
    public String ybackup;

    CommandPattern(Aardvark editor) {
        this.editor = editor;
    }

    void backup() {
        xbackup = editor.xvalue;
        ybackup = editor.yvalue;
    }

    public void undo() {
        editor.xvalue = xbackup;
        editor.yvalue = ybackup;
    }

    public abstract boolean execute();
}







