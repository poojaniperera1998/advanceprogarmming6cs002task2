import java.util.Stack;

public class OldCommand {
	private Stack<CommandPattern> history = new Stack<>();

	public void push(CommandPattern command) {
		// TODO Auto-generated method stub
		
		history.push(command);
	}
	
    public CommandPattern pop() {
        return history.pop();
    }

    public boolean isEmpty() { 
    	return history.isEmpty(); 
    	}

}
