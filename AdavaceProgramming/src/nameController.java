import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class nameController {

	private gui1 theView;
	private name theModel;
	private GUI view;

	public nameController(gui1 theView, name theModel) {
		
		this.theView = theView;
		this.theModel = theModel;
		
		this.theView.addNameListener(new Player());
	
	}
	
	class Player implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String name = "";
			name = theView.getname();
			theModel.setName(name);
			
			if(name.isEmpty()) {
				theView.displayErrorMessage("Please Enter Your Name");
			}else {
				
				theView.setName(theModel.getName());
				
				GUI menu = new GUI();
				menu.PlayerName (theModel.getName());
				menu.setVisible(true);
			}

		}
		
	}
	
}
