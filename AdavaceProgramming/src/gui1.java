import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
public class gui1 extends JFrame {

	private JPanel panel=new JPanel(new FlowLayout());

	private JLabel label=new JLabel("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe"
			+ "Version 1.0 (c), Kevan Buckley, 2010");
	private JTextField textfield=new JTextField("Enter your name");
	private JButton Button=new JButton("Enter");
	public String player = null;
gui1(){
		
			setSize(800,300);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLocationRelativeTo(null);
			setLayout(new GridLayout(2,1));
		
			panel.add(textfield);
			textfield.setToolTipText("Enter Your Name Here");
			textfield.setFont(new Font(Font.DIALOG_INPUT, Font.PLAIN, 12));
		    textfield.setForeground(Color.BLACK);

			textfield.setHorizontalAlignment(SwingConstants.RIGHT);
			textfield.setFont(new Font("",1,20));
			textfield.setPreferredSize( new Dimension( 200, 24));
			
			panel.add(Button);
			
			label.setForeground(Color.RED);
		    label.setVerticalAlignment(SwingConstants.TOP);
		   
			add(panel);
			add(label);
			
		
}
		
		public String getname(){
			return textfield.getText();

		}
		
		public void setName(String player) {
			JOptionPane.showMessageDialog(this, "Your Name " + player);
			
		}
		
		public void addNameListener(ActionListener listenForButton){
			Button.addActionListener(listenForButton);

		}


		public void displayErrorMessage(String errorMessage){
			JOptionPane.showMessageDialog(this, errorMessage);

		}


}
